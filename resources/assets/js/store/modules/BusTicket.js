import {BUS_TABS} from '../../consts/service'

const state = {
    chartResponse: {
      Pickups:[],
      Dropoffs:[],
    },
    pickup_selected: {},
    dropoff_selected: {},
    tab_active: BUS_TABS.pickup,
    loadingDetail: false,
    money: 0,
}

const mutations = {
  setChartResponse(state, data) {
    state.chartResponse = data
  },

  selectedPickup(state, data) {
    state.pickup_selected = data
  },

  selectedDropoff(state, data) {
    state.dropoff_selected = data
  },

  setTabActive(state, tab) {
    state.tab_active = tab
  },

  changeLoading(state, value) {
    state.loadingDetail = value
  },

  changeMoney(state, value) {
    state.money = value
  },
}

const actions = {
  setChartResponse ({ commit }, data) {
    // do something async
    commit('setChartResponse', data)
  },

  selectedPickup ({ commit }, data) {
    // do something async
    commit('selectedPickup', data)
  },

  selectedDropoff ({ commit }, data) {
    // do something async
    commit('selectedDropoff', data)
  },

  setTabActive ({ commit }, data) {
    // do something async
    commit('setTabActive', data)
  },

  changeLoading ({ commit }, data) {
    // do something async
    commit('changeLoading', data)
  },
  
  changeMoney ({ commit }, data) {
    // do something async
    commit('changeMoney', data)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
