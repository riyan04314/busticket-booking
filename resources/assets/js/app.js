
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Autocomplete from 'v-autocomplete';
import 'v-autocomplete/dist/v-autocomplete.css';
import VModal from 'vue-js-modal';
import store from './store';
import mixin from './mixin';
import VTooltip from 'v-tooltip'

window.Vue = require('vue');

Vue.mixin(mixin);
Vue.use(Autocomplete);
Vue.use(VModal, { dialog: true })
Vue.use(VTooltip);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
//services
Vue.component('services-bus', require('./components/services/busticket.vue'));
//Modals
Vue.component('bus-modal', require('./components/modals/bus_modal.vue'));

Vue.component('s-banner', require('./components/banner.vue'));


const app = new Vue({
    el: '#app',
    store
});
