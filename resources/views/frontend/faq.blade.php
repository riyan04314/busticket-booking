@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('css')
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection

@section('innerbanner')

    <!-- innerbanner -->

    <div class="banner">
        <s-banner></s-banner>
    </div>

    <!-- //innerbanner -->

@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection

@section('vertical tab')


    <div class="w3layouts-breadcrumbs text-center">
        <div class="container">
            <span class="agile-breadcrumbs"><a href="/"><i
                            class="fa fa-home home_1"></i></a> / <span>Faq</span></span>
        </div>
    </div>
    <!-- //breadcrumbs -->
    <!-- FAQ-help-page -->
    <div class="help about w3layouts-content">
        <div class="container">
            <h3 class="w3-head">How can we help you</h3>
            <div class="faq-w3agile">
                <h5>Top 10 Frequently asked questions(FAQ)</h5>
                <ul class="faq">
                    <li class="item1"><span>How to Login if I Forget my Password?</span>
                        <ul>
                            <li class="subitem1"><p>
                                    Use the Forgot your password option on the Login form to set your new password.</p>
                            </li>
                        </ul>
                    </li>
                    <li class="item1"><span>How to Login if I forget my Registered Email ID?</span>
                        <ul>
                            <li class="subitem1"><p>
                                    You can mail us at <a href="mailto:care@zapwallet.in">care@zapwallet.in</a> saying your details like verified mobile number
                                    with zapwallet, your name and city. You will get your registered Email ID in the
                                    next 24-72 hours.</p></li>
                        </ul>
                    </li>

                    <li class="item1"><span>How to Get Refund of wallet balance to Bank?</span>
                        <ul>
                            <li class="subitem1"><p>
                                    No. You cannot get refund of your wallet balance to your bank account. You can only
                                    use your wallet balance to do transactions in the https://www.zapwallet.in website
                                    alone.</p></li>
                        </ul>
                    </li>

                    <li class="item1">
                        <span>How to get back the Deducted money of the failed transaction to my wallet?</span>
                        <ul>
                            <li class="subitem1"><p>
                                    3 – 21 working days. You have to send an email to <a href="mailto:care@zapwallet.in">care@zapwallet.in</a> with proper
                                    details and evidences to get your money back.
                                </p></li>
                        </ul>
                    </li>
                    <li class="item1">
                        <span>Is it Possible to cancel the Bus tickets booked in the zapwallet website?</span>
                        <ul>
                            <li class="subitem1"><p>
                                    Yes. But the cancellation depends on the time gap you take between booking a ticket
                                    and cancelling ticket. Based on the time gap, the cancellation system and terms
                                    vary.
                                </p></li>
                        </ul>
                    </li>

                </ul>
                <br>
                In case of more queries, mail us at <a href="mailto:care@zapwallet.in">care@zapwallet.in</a>
                <Br>

                In case of any feedback or suggestions, mail us at <a href="mailto:feedback@zapwallet.in">feedback@zapwallet.in</a>
                <Br>
                Thanks.


                <!-- script for tabs -->
                <script type="text/javascript">
                    $(function () {

                        var menu_ul = $('.faq > li > ul'),
                            menu_a = $('.faq > li > a');

                        menu_ul.hide();

                        menu_a.click(function (e) {
                            e.preventDefault();
                            if (!$(this).hasClass('active')) {
                                menu_a.removeClass('active');
                                menu_ul.filter(':visible').slideUp('normal');
                                $(this).addClass('active').next().stop(true, true).slideDown('normal');
                            } else {
                                $(this).removeClass('active');
                                $(this).next().stop(true, true).slideUp('normal');
                            }
                        });

                    });
                </script>
                <!-- script for tabs -->
            </div>
        </div>
    </div>
    <!-- //FAQ-help-page -->

@endsection

@section('tab title')
    Plan
@endsection

<!-- subscribe -->

<!-- //subscribe -->

<!--footer-->
