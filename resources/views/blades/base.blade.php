<!DOCTYPE html>
<html>

<!-- head -->

<head>

    <title>@yield('page title', 'Zapwallet -  Mobile Recharge, DTH Recharge & Bus Tickets.')</title>

    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all"/><!-- bootstrap-CSS -->
@yield('css')
{{--<link rel="stylesheet" href="{{asset('css/bootstrap-select.css')}}">--}}<!-- bootstrap-select-CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" type="text/css" media="all"/><!-- Fontawesome-CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/easy-responsive-tabs.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/style.css?v='. time() )}}" rel="stylesheet" type="text/css" media="all"/><!--theme-style-->
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/datapicker/css/bootstrap-datepicker.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}"/>
    <style>
        .mobile-number input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            display: none;
        }

        .table th {
            text-align: center;
        }
    </style>
                                                                                              <!--//theme-style-->

                                                                                              <!-- metatags -->
                                                                                              <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <meta name="keywords" content="Zapwallet Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,

	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design"/>

    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese"
          rel="stylesheet">

    <link href="//fonts.googleapis.com/css?family=Oxygen:300,400,700&amp;subset=latin-ext" rel="stylesheet">

    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
          rel="stylesheet">
    <link rel="shortcut icon" href="/Zapwallet_180717-01.png" type="image/png">
                                                                                              <!-- online fonts -->
    @yield('css')
</head>

<!-- /head -->

<!-- body -->

<body>
@php( $not_reset_password = !in_array(\Request::route()->getName(), ['password.request', 'password.reset']))
<div class="bg">
    <div class="bubblingG">
        <img src="/images/logo.png" /><br>
        <span id="bubblingG_1"></span>
        <span id="bubblingG_2"></span>
        <span id="bubblingG_3"></span>
    </div>
</div>
<div id="app">

    <!--header-->
@include('blades.header')
<!-- //header -->
@yield('innerbanner')
@yield('breadcrumbs')
@yield('header-right')
@yield('horizontal tab')
@yield('vertical tab')
<!-- subscribe -->
@include('blades.subscribe')
<!-- //subscribe -->
    <!--footer-->
    @include('blades.footer')
</div>
<!--//footer-->

<!-- for bootstrap working -->
<script type='text/javascript' src="{{asset('js/app.js')}}"></script>
<script src="//code.jquery.com/jquery-3.3.1.min.js" ></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('plugins/datapicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/responsiveslides.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
{{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>--}}
<script src="{{asset('js/easyResponsiveTabs.js')}}"></script>
<script src="{{asset('js/bootstrap-select.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.selectpicker').selectpicker();
    });

</script>

<script type="text/javascript" src="{{asset('js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('js/easing.js')}}"></script>
<script type="text/javascript" src="{{asset('js/home.js')}}"></script>
<script type="text/javascript" src="{{asset('js/xml2json.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

            preloaderFadeOutTime = 1000;

            function hidePreloader() {
                var preloader = $('.bg');
                preloader.fadeOut(preloaderFadeOutTime);
            }

            hidePreloader();

        var prepaidprovider = undefined;
        var prepaidnumber;
        var prepaidcircle;
        var postpaidprovider = undefined;
        var postpaidnumber;

        $("#selectpicker").selectpicker();

        $(".scroll").click(function (event) {

            event.preventDefault();

            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
        $("#prepaidselect").change(function () {
            prepaidprovider = $("#prepaidselect").val();
            console.log(prepaidprovider)

        });
        $("#postpaidselect").change(function () {
            postpaidprovider = $("#postpaidselect").val();
            console.log('postpaidprovider ' + postpaidprovider)

        });
        $("#prepaidtel").keyup(function () {

            let tel = $("#prepaidtel").val();
            //console.log(tel);
            if (tel.length == 10) {
                $.ajax({
                    type: "GET",
                    url: 'https://www.rupeebiz.com/apiservice.asmx/GetOperator?mn=' + tel,
                    dataType: "xml",
                    success: function (xmlDoc) {
                        var x2js = new X2JS();
                        var jsonObj = x2js.xml2json(xmlDoc);
                        if (jsonObj.MobileProvider.provider != undefined) {
                            prepaidprovider = jsonObj.MobileProvider.provider.toString();
                            prepaidcirckle = jsonObj.MobileProvider.state.toString();
                            prepaidnumber = tel;
                            $("#prepaid>div>select>option").each(function () {
                                $(this).removeAttr("selected", true)
                            });
                            $("#prepaidcircle>div>select>option").each(function () {
                                $(this).removeAttr("selected", true)
                            });
                            $("#prepaid>div>select>option").each(function () {
                                prov = $(this).text();
                                //console.log(prov+' '+prepaidprovider);
                                if (prov.indexOf(prepaidprovider) !=-1 ) {
                                    $(this).attr("selected", true)
                                }
                                //console.log(prov.indexOf(prepaidprovider))
                            });
                            $("#prepaidcircle>option").each(function () {
                                text = this.innerText;
                                console.log(prepaidcirckle.indexOf(text));
                                if (prepaidcirckle.indexOf(text)!= -1&& prepaidcirckle.indexOf(text)<20) {
                                    $(this).attr("selected", true)
                                }
                            });
                            $(".selectpicker").selectpicker('refresh');

                            console.log(prepaidprovider);
                            console.log(prepaidcirckle)
                        } else {
                            console.log(prepaidprovider)

                        }
                    }
                });

            }
        });
        $("#postpaidtel").keyup(function () {

            let tel = $("#postpaidtel").val();
            if (tel.length == 10) {
                $.ajax({
                    type: "GET",
                    url: 'https://www.rupeebiz.com/apiservice.asmx/GetOperator?mn=' + tel,
                    dataType: "xml",
                    success: function (xmlDoc) {
                        var x2js = new X2JS();
                        var jsonObj = x2js.xml2json(xmlDoc);
                        postpaidprovider = jsonObj.MobileProvider.provider.toString();
                        postpaidnumber = tel;

                        console.log(postpaidprovider);
                        $("#postpaid>div>select>option").each(function () {
                            $(this).removeAttr("selected", true)
                        });
                        $("#postpaid>div>select>option").each(function () {
                            if ($(this).text() == postpaidprovider /*+ ' Postpaid'*/) {
                                //this.remove()
                                $(this).attr("selected", true)
                            }
                        });
                        $(".selectpicker").selectpicker('refresh')
                    }
                });
            }
        });
        $("#dthid").keyup(function () {

            let tel = $("#dthid").val();
            console.log(tel);
            if (tel.length >= 6) {
                $.ajax({
                    type: "GET",
                    url: 'https://www.rupeebiz.com/apiservice.asmx/GetOperator?mn=' + tel,
                    dataType: "xml",
                    success: function (xmlDoc) {
                        var x2js = new X2JS();
                        var jsonObj = x2js.xml2json(xmlDoc);
                        provider = jsonObj.MobileProvider.provider.toString();
                        console.log(provider);
                        $("#dth>div>select>option").each(function () {
                            $(this).removeAttr("selected", true)
                        });
                        $("#dth>div>select>option").each(function () {
                            if ($(this).text() == provider + ' Postpaid') {
                                $(this).attr("selected", true)
                            }
                        });
                        $(".selectpicker").selectpicker('refresh')
                    }
                });
            }
        });
        /*
        $("#prepaidbutton").click(function (event) {
            event.preventDefault();
            $('#prepaidresult').remove();
            $('#postpaidresult').remove();
            /!*order_id = 'ZAP-' + new Date();*!/
            tel=$('#prepaidtel').val();
            prepaidprovider = $('#prepaidselect').val();
            prepaidcircle = $('#prepaidcircle').val();
            prepaidamount = $("#prepaidamount").val();
            if (prepaidcircle != "Select Circle" && prepaidamount) {
                $('.tab-grids').after(function () {
                    return '<div id="prepaidresult"><table class="table"><thead><tr style="background-color: #0089cf"><td style="color: #fff" scope="col">Type</td><td style="color: #fff" scope="col">Operator</td><td style="color: #fff" scope="col">Amount</td><td style="color: #fff" scope="col">Wallet balance</td><td style="color: #fff" scope="col">Pay</td></tr></thead><tbody><tr><th scope="row">' + prepaidtype + '</th><th>' + prepaidprovider + '</th><th>' + prepaidamount + '</th><th class="class="text-center""><input type="checkbox" id="prepaidwallet"/></th><th><form method="post" name="customerData" action="/mobilerecharge"><input type="hidden"  value="tid" name="tid" id="tid" readonly /><input type="hidden"  name="merchant_id" value="112068"/><input type="hidden" name="tel" value="' + tel + '"/><input type="hidden"  name="amount" value="' + prepaidamount + '"/><input type="hidden"  name="prepaidprovider" value="' + prepaidprovider + '"/><input type="hidden"  name="prepaidcircle" value="' + prepaidcircle + '"/><input type="hidden" name="currency" hidden value="INR"/><input type="hidden" name="redirect_url" value="http://zapwallet.in/ccavResponseHandler"/><input type="hidden" name="cancel_url" value="http://zapwallet.in/ccavResponseHandler"/><input type="hidden" name="language" value="EN"/><input type="submit" class="submit" id="prepaidclick" value="Pay now"></form></th></tr></tbody></table></div>';
                });
            }
            else {
                alert("Please fill all fields");
            }

        });
*/
        /*$("#postpaidbutton").click(function (event) {
            event.preventDefault();
            $('#postpaidresult').remove();
            $('#prepaidresult').remove();

            postpaidcircle = $('#postpaidcircle').val();
            postpaidamount = $("#postpaidamount").val();
            $('.tab-grids').after(function () {
                return '<div id="postpaidresult"><table class="table"><thead><tr style="background-color: #0089cf"><td style="color: #fff" scope="col">Type</td><td style="color: #fff" scope="col">Operator</td><td style="color: #fff" scope="col">Amount</td><td style="color: #fff" scope="col">Wallet balance</td><td style="color: #fff" scope="col">Pay</td></tr></thead><tbody><tr><th scope="row">' + postpaidtype + '</th><th>' + postpaidprovider + '</th><th>' + postpaidamount + '</th><th class="class="text-center""><input type="checkbox" id="prepaidwallet"/></th><th><input type="submit" class="submit" id="postpaidclick" value="Pay now"></th></tr></tbody></table></div>';
            });
        });*/
    });

    "@if (sizeof(@$errors)>0 && $not_reset_password)"
    $(".w3layouts-login a").click();
    $("#Registration").addClass('active');
    $("#Login").removeClass('active');
    $(".extra-w3layouts ul li:nth-child(2)").addClass('active');
    $(".extra-w3layouts ul li:nth-child(1)").removeClass('active');
    "@endif"
</script>

@yield('scripts')
</body>

<!-- /body -->

</html>

<!-- /html -->