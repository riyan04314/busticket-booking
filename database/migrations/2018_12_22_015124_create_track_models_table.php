<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_models', function (Blueprint $table) {
           
            $table->increments('id');
            $table->integer('tracking_no')->nullable();
            $table->string('destination')->nullable();
            $table->string('s_company_name')->nullable();
            $table->string('s_location')->nullable();
            $table->string('r_name')->nullable();
            $table->string('r_adress')->nullable();
            $table->string('r_contact')->nullable();
            $table->integer('sh_no')->nullable();
            $table->integer('sh_cod_amount')->nullable();
            $table->integer('sh_cus_value')->nullable();
            $table->string('sh_loc')->nullable();
            $table->string('created_by')->nullable();
            $table->string('good_des')->nullable();
            $table->string('weight')->nullable();
            
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_models');
    }
}
