<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Dthconntransaction extends Model
{
    protected $fillable = [
      'order_id',
      'currency',
      'amount',
      'mn'
    ];
    public function users (  ) {
        return $this->belongsTo('App\User');
    }
}
