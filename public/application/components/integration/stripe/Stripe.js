(function(){
    "use strict";

    angular.module('Stripe', ['ui.bootstrap', 'ngAnimate'])

    .factory('StripeConfigRepository', StripeConfigRepository)

    .directive('stripeIntegration', StripeIntegration)

    function StripeIntegration(BASE, BlockUI) {
        return {
            restrict: 'EA',
            controller: StripeIntegrationController,
            templateUrl: BASE.API_URL + BASE.ASSETS_URL + 'integration/stripe/templates/index.html',
            link: function (scope, iElement, iAttrs) {

            }
        };
    }

    function StripeIntegrationController(BASE, $scope, $timeout, BlockUI, StripeConfigRepository) {
        
        $scope.show_form = false;

        $scope.stripe = {};
        $scope.isLoading = true;
        $scope.has_keys = false;

        $timeout(function(){
            BlockUI.open('#stripe-integration', '<div class=" fa-spin flaticon-refresh"></div> Stripe loading...');
            $scope.loadStripe();
        });

        $scope.loadStripe = function(){

            StripeConfigRepository.get().then(function(response){

                let response_data = response.data.success.data;

                BlockUI.close('#stripe-integration');
                $scope.has_keys = true;
                $scope.isLoading = false;
                $scope.stripe = response_data;

            }, function(){

                BlockUI.close('#stripe-integration');
                $scope.has_keys = false;
                $scope.isLoading = false;
            });
        }

        $scope.reauth = function(){
        }

        $scope.integrateStripe = function(){

            $scope.isLoading = true;
            BlockUI.open('#stripe-integration', '<div class=" fa-spin flaticon-refresh"></div> Integrating Stripe...');

            StripeConfigRepository.store($scope.stripe).then(function(response){

                let response_data = response.data;

                $scope.show_form = false;
                $scope.has_keys = true;
                $scope.isLoading = false;
                BlockUI.close('#stripe-integration');

                $scope.loadStripe()

                swal({
                    position:"center",
                    type:"success",
                    title:'Stripe Credentials',
                    html: response_data.success.message,
                    showConfirmButton:!1,
                    timer:5000
                });

            }, function(response){
                
                let response_error = response.data;

                let html = '<div class="m-demo"><div class="m-demo__preview"><div class="m-list-timeline"><div class="m-list-timeline__items">';

                if (response_error.errors) {

                    $.each(response_error.errors, function(index, val) {
                         '<li>' + val[0] + '</li>';

                         html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                         html += val[0];
                         html += '</span></div></div></div>';
                    });

                }

                if (response_error.error) {

                     '<li>' + response_error.error.message + '</li>';

                     html += '<div class="m-list-timeline"><div class="m-list-timeline__items"><div class="m-list-timeline__item"><span class="m-list-timeline__icon flaticon-cancel"></span><span class="m-list-timeline__text">';
                     html += response_error.error.message;
                     html += '</span></div></div></div>';
        
                }

                html += '</div></div></div></div>';

                swal({
                    position:"center",
                    type:"warning",
                    title: response_error.message,
                    html: html,
                    showConfirmButton:!1,
                    timer:5000
                });

                $scope.show_form = false;
                $scope.has_keys = false;
                $scope.isLoading = false;
                BlockUI.close('#stripe-integration');

            })

        }

        $scope.updateKeys = function(){
            $scope.show_form = true;
        } 
    }

    function StripeConfigRepository(BASE, $http) {
        
        let url = BASE.API_URL + BASE.API_VERSION + 'admin/stripe/config';

        let repo = {};

        repo.get = function() {
            return $http.get(url);
        }

        repo.store = function(params) {
            return $http.post(url, params);
        }

        return repo;

    }
})();