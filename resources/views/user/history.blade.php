@extends('blades.base')

{{-- @section('page title', 'Index') --}}



@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection
@section('css')
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/tool-tip.css')}}">
    <link rel="stylesheet" href="{{ asset('css/history.css')}}">
@endsection
@section('vertical tab')
    @if (session('status'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
    @endif
    <!--Vertical Tab-->

    <div class="container-fluid order-head" style="padding-top: 130px">
        <div class="container">
            <div class="row">
                <div class="col-md-3 walet-sec">
                    <img src="{{asset('images/history/order-history.png')}}" class="img-responsive"/>
                    <h2>Rs {{Auth::user()->wallet}}</h2>
                    <p>Your Wallet Balance</p>
                </div>

                    <form action="/walletrecharge" name="customerData" method="post" class="signup">
                        {{ csrf_field() }}
                        <div class="col-md-5 input-sec">
                            <input type="text" required name="amount" value="{{ old('amount') }}" placeholder="Enter amount to be added in your wallet"/>
                            <p><a href="#">Have a Promo code?</a></p>
                        </div>
                        <input type="hidden" name="billing_name" value="{{$user->name}}"/>
                        <input type="hidden" name="billing_address" value="{{ $user->address }}"/>
                        <input type="hidden" name="billing_city" value="{{$user->city}}"/>
                        <input type="hidden" name="billing_state" value="MP"/>
                        <input type="hidden" name="billing_zip" value="425001"/>
                        <input type="hidden" name="billing_country" value="{{$user->country}}"/>
                        <input type="hidden" name="billing_tel" value="{{$user->mobile}}"/>
                        <input type="hidden" name="billing_email" value="{{$user->email}}"/>
                        <input type="hidden" name="merchant_id" value="112068"/>
                        <input type="hidden" name="order_id" value="ZAP-{{ time() }}"/>
                        <input type="hidden" name="currency" value="INR"/>
                        <input type="hidden" name="redirect_url" value="http://zapwallet.in/ccavResponseHandler"/>
                        <input type="hidden" name="cancel_url" value="http://zapwallet.in/ccavResponseHandler"/>
                        <input type="hidden" name="language" value="EN"/>
                        <div class="col-md-4 view-button">
                            <button style="display:none;" type="button" class="btn  btn-lg">Recharge now</button>
                            <button type="submit" class="btn  btn-lg">Add Money to Wallet</button>
                        </div>
                    </form>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
    <div class="container order-sec" style="padding-top: 110px">
        <div class="order-top text-center"><img src="/images/logo.png"/></div>
        <div class="text-center order-body" style="padding: 30px">

            <div class="ul-border">
                <ul class="nav nav-tabs" role="tablist">
                    {{--<li role="presentation" class="active">
                        <a href="#all" aria-controls="all" role="tab" data-toggle="tab">All Order</a></li>--}}
                    <li role="presentation" class="active">
                        <a href="#mobile-prepaid" aria-controls="mobile-prepaid" role="tab" data-toggle="tab">Mobile-Prepaid</a>
                    </li>
                    <li role="presentation">
                        <a href="#mobile-postpaid" aria-controls="mobile-postpaid" role="tab" data-toggle="tab">Mobile-Postpaid</a>
                    </li>
                    <li role="presentation"><a href="#dth" aria-controls="dth" role="tab" data-toggle="tab">DTH</a></li>
                    {{--<li role="presentation">
                        <a href="#datacard" aria-controls="datacard" role="tab" data-toggle="tab">Datacard</a></li>
                    <li role="presentation">
                        <a href="#landline" aria-controls="landline" role="tab" data-toggle="tab">Landline</a></li>--}}
                    <li role="presentation">
                        <a href="bus" aria-controls="bus" role="tab" data-toggle="tab">Bus ticket</a>
                    </li>
                    <li role="presentation">
                        <a href="#cashback" aria-controls="cashback" role="tab" data-toggle="tab">Cashback</a>
                    </li>
                </ul>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                {{--<div role="tabpanel" class="tab-pane fade in active" id="all">
                    <div class="order-table">
                        <table>
                            <tbody>
                            <th>Product description</th>
                            <th>Status</th>
                            <th>Price</th>
                            <th>Coupon</th>
                            <th>Order No.</th>
                            <th>Order Total</th>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill Payment of Idea Mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-rd"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Payment failed
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>--}}
                <div role="tabpanel" class="tab-pane fade in active" id="mobile-prepaid">
                    <div class="order-table">
                        <table>
                            <tbody>
                            <th>Product description</th>
                            <th>Status</th>
                            <th>Price</th>
                            <th>Coupon</th>
                            <th>Order No.</th>
                            <th>Order Total</th>
                            @foreach($arr['Prepaid'] as $transaction)
                                <tr>
                                    <td>
                                        Bill payment of mobile {{$transaction->mn}}
                                    </td>
                                    <td @if($transaction->status =='SUCCESS')
                                        class="status-gr"
                                        @else
                                        class="status-rd"
                                            @endif
                                    >
                                        <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is {{$transaction->status}}
                                    </td>
                                    <td>Rs. {{$transaction->amount}}</td>
                                    <td>0 Rs</td>
                                    <td>{{$transaction->order_id}} <br/>
                                        {{$transaction->created_at->toFormattedDateString()}}, {{$transaction->created_at->toTimeString()}}
                                    </td>
                                    <td>Rs. {{$transaction->amount}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="mobile-postpaid">
                    <div class="order-table">
                        <table>
                            <tbody>
                            <th>Product description</th>
                            <th>Status</th>
                            <th>Price</th>
                            <th>Coupon</th>
                            <th>Order No.</th>
                            <th>Order Total</th>
                            @foreach($arr['Postpaid'] as $transaction)
                                <tr>
                                    <td>
                                        Bill payment of idea mobile {{$transaction->mn}}
                                    </td>
                                    <td @if($transaction->status =='SUCCESS')
                                        class="status-gr"
                                        @else
                                        class="status-rd"
                                            @endif
                                    >
                                        <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is {{$transaction->status}}
                                    </td>
                                    <td>Rs. {{$transaction->amount}}</td>
                                    <td>0Rs</td>
                                    <td>{{$transaction->order_id}} <br/>
                                        {{$transaction->created_at->toFormattedDateString()}}
                                    </td>
                                    <td>Rs. {{$transaction->amount}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="dth">
                    <div class="order-table">
                        <table>
                            <tbody>
                            <th>Product description</th>
                            <th>Status</th>
                            <th>Price</th>
                            <th>Coupon</th>
                            <th>Order No.</th>
                            <th>Order Total</th>
                            @foreach($arr['Dth'] as $transaction)
                                <tr>
                                    <td>
                                        Bill payment of idea mobile {{$transaction->mn}}
                                    </td>
                                    <td @if($transaction->status =='SUCCESS')
                                        class="status-gr"
                                        @else
                                        class="status-rd"
                                            @endif>
                                        <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is {{$transaction->status}}
                                    </td>
                                    <td>Rs. {{$transaction->amount}}</td>
                                    <td>0Rs</td>
                                    <td>{{$transaction->order_id}} <br/>
                                        {{$transaction->created_at->toFormattedDateString()}}
                                    </td>
                                    <td>Rs. {{$transaction->amount}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="cashback">
                    <div class="order-table">
                        <table>
                            <tbody>
                            <th>Product Description</th>
                            <th>Amount</th>
                            <th>Coupon</th>
                            <th>Date</th>
                            @foreach($arr['cashback'] as $cb)
                                <tr>

                                    <td>
                                        {{ $cb->remark }}
                                    </td>
                                    <td>Rs. {{$cb->amount}}</td>
                                    <td>0Rs</td>
                                    <td>
                                        {{$cb->created_at}}
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                {{--<div role="tabpanel" class="tab-pane fade" id="datacard">

                    <div class="order-table">
                        <table>
                            <tbody>
                            <th>Product description</th>
                            <th>Status</th>
                            <th>Price</th>
                            <th>Coupon</th>
                            <th>Order No.</th>
                            <th>Order Total</th>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill Payment of Idea Mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-rd"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Payment failed
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane fade" id="landline">

                    <div class="order-table">
                        <table>
                            <tbody>
                            <th>Product description</th>
                            <th>Status</th>
                            <th>Price</th>
                            <th>Coupon</th>
                            <th>Order No.</th>
                            <th>Order Total</th>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill Payment of Idea Mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-rd"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Payment failed
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane fade" id="bus">

                    <div class="order-table">
                        <table>
                            <tbody>
                            <th>Product description</th>
                            <th>Status</th>
                            <th>Price</th>
                            <th>Coupon</th>
                            <th>Order No.</th>
                            <th>Order Total</th>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill Payment of Idea Mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-rd"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Payment failed
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            <tr>
                                <td>
                                    <img src="img/idea.png" alt="" class="img-responsive"/> &nbsp; Bill payment of idea mobile 9726300605
                                </td>
                                <td class="status-gr">
                                    <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your order is successful
                                </td>
                                <td>Rs. 350</td>
                                <td>.50Rs</td>
                                <td>3572430893 <br/>
                                    4.26 pm ,jul 25 2017
                                </td>
                                <td>Rs. 300</td>

                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>--}}
            </div>
        </div>
        <div class="clearfix"></div>
        <!--order_footer-->
        {{--<div class="col-md-12 order-footer text-center">
            <a href="#" class="login-button sign-button">View more</a>
        </div>--}}
        <div class="clearfix"></div>
    </div>
@endsection
@section('tab title')
    Plan
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            //Vertical Tab
            $('#parentVerticalTab').easyResponsiveTabs({
                type: 'vertical', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo2');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $("#tab2").hide();
            $("#tab3").hide();
            $("#tab4").hide();
            $(".tabs-menu a").click(function (event) {
                event.preventDefault();
                var tab = $(this).attr("href");
                $(".tab-grid").not(tab).css("display", "none");
                $(tab).fadeIn("slow");
            });
        });
    </script>
    <script src="/js/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#datepicker,#datepicker1").datepicker();
        });
    </script>
@endsection