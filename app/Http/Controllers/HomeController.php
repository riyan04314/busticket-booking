<?php

namespace App\Http\Controllers;

use App\Classes\Wallet;
use App\models\Opcode;
use App\models\Prepaidoperator;
use Auth;
use Illuminate\Http\Request;
use Mail;
class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct () {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        //if($this->isVerified()) {
        return view('/');
        //}else{
        //return redirect('verifyOtp');
        //}
    }

    public function aboutus () {

        return view('frontend.about');
    }

    public function faq () {

        return view('frontend.faq');
    }

    public function bus () {

        return view('frontend.bus');
    }

    public function support () {

        return view('frontend.support');
    }
    public function supportPost (Request $request) {
        $r = $request->all();
        $emailAr = ['r' => $r];
        Mail::send('emails.support', $emailAr, function ($message) use ($emailAr) {
            $message->to('care@zapwallet.in')->subject('Support');
        });
        Mail::send('emails.support', $emailAr, function ($message) use ($emailAr) {
            $message->to('skladopt.com@gmail.com')->subject('Support');
        });
        dd(config('mail'));
        return redirect()->route('frontend.support');
    }

    public function sitemap () {

        return view('frontend.sitemap');
    }

    public function terms () {
        return view('frontend.terms');
    }

    public function privacyPpolicy () {
        return view('frontend.privacy_policy');
    }

    public function refundPolicy () {
        return view('frontend.refund_policy');
    }


    public function isVerified () {
        $user = Auth::user();
        if ( $user->isVerified == '1' ) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function feedback () {

        return view('frontend.feedback');
    }
    public function feedbackPost (Request $request) {
        $r = $request->all();
        $emailAr = ['r' => $r];

        Mail::send('emails.feedback', $emailAr, function ($message) use ($emailAr) {
            $message->to('feedback@zapwallet.in')->subject('Feedback');
        });
        return redirect()->route('feedback');
    }

    public function contact () {

        return view('frontend.contact');
    }

    public function mobilerecharge ( Request $request ) {
        $user = Auth::user();
        //dd($request->input());
        $rechargeamount = $request->input('amount');
        $tel            = $request->input('tel');
        $operator       = $request->input('oprator');
        //dd($operator);
        $fromwallet     = $request->input('fromwallet');
        $encrypted_data = Wallet::recharge_mobile($user, $rechargeamount, 'INR', $tel, $operator, $fromwallet);
        $access_code    = 'AVOQ72EH49BC73QOCB';
            //dd($encrypted_data );

        return view('ccavredirectform', compact('encrypted_data', 'access_code'));

    }

    public function walletrecharge ( Request $request ) {
        $user           = Auth::user();
        $amount         = $request->input('amount');
        $encrypted_data = Wallet::recharge_wallet($user, 'INR', $amount);
        $access_code    = 'AVOQ72EH49BC73QOCB';

        //dump($encrypted_data);
        return view('ccavredirectform', compact('encrypted_data', 'access_code'));

    }

    public function dowalletRechargeBalance($amount)
    {
        $user           = Auth::user();
        $encrypted_data = Wallet::recharge_wallet($user, 'INR', $amount);
        $access_code    = 'AVOQ72EH49BC73QOCB';

        //dump($encrypted_data);
        return view('ccavredirectform', compact('encrypted_data', 'access_code'));
    }

    public function mobilerechargemiddle ( Request $request ) {
        $prepaidprovider = $request->input('oprator');
        $type = $request->input('type');
        $operatorname    = Opcode::where('opcode', $request->input('oprator'))->first()->name;
        $prepaidamount   = $request->input('amount') ? $request->input('amount') : 1;
        $tel             = $request->input('tel');
        $fromwallet      = $request->input('fromwallet');

        $walletBalance = Auth::user()->wallet;


        if ($walletBalance < $prepaidamount)
        {
            $amount = $prepaidamount - $walletBalance;
            return $this->dowalletRechargeBalance($amount);
        }else{

            return view('forms.rechargemobileform', compact('prepaidprovider', 'prepaidamount', 'tel', 'fromwallet', 'operatorname','type'));
        }

    }
    
    public function electricityBilling ( Request $request ) {
        $prepaidprovider = $request->input('oprator');
        $type = $request->input('type');
        $operatorname    = Opcode::where('opcode', $request->input('oprator'))->first()->name;
        $prepaidamount   = $request->input('amount') ? $request->input('amount') : 1;
        $account_number = $request->input('tel');
        $tel = $request->input('tel');
        $fromwallet      = $request->input('fromwallet');

        $walletBalance = Auth::user()->wallet;


        if ($walletBalance < $prepaidamount)
        {
            $amount = $prepaidamount - $walletBalance;
            return $this->dowalletRechargeBalance($amount);
        }else{
            
            $location = 'https://www.freecharge.in/rest/bill/fetchBill';
            
            ## define your user agent
            $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
            
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_VERBOSE, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_URL, $location );
                
            $post_array = array(
                'serviceProvider' => $request->input('oprator'),
                'additionalInfo1' => $account_number,
                'serviceRegion' => '-1',
            );
            
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_array); 
            ## grab the response from the remote form processor
            
                $response = curl_exec($ch);
                ## echo resonse or save as text or xml file. The choice are just too many to list. It is all up to you.
            
                
                ## define what are you going to do if the remote processor sent out a fault response
                if($response == "records not available"){
                echo "roll_number is not valid";
                ## you can show your form here again default
                }   
            }


            return view('forms.rechargeelectricform', compact('prepaidprovider', 'prepaidamount', 'tel', 'fromwallet', 'operatorname','type', 'response'));
        

    }
    
    public function ccAvenueCancel()
    {
        die('Transaction was cancelled');
    }

}
