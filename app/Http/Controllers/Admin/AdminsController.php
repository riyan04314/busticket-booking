<?php

namespace App\Http\Controllers\Admin;

use App\models\Dthtransaction;
use App\models\Mobiletransaction;
use App\models\Postpaidtransaction;
use App\models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AdminsController extends Controller
{

    public function dashboard()
    {
        $mobileTransactions = Mobiletransaction::all();
        $dthTransactions = Dthtransaction::all();
        $postPaidTransactions = Postpaidtransaction::all();
        $walletTransactions = Transaction::all();
        $users = User::all();

        $successMobile = Mobiletransaction::where('status','SUCCESS')->get();
        $successPostPaid = Postpaidtransaction::where('status','SUCCESS')->get();
        $successDth = Dthtransaction::where('status','SUCCESS')->get();
        return view('admin.dashboard',[
            'mobileTransactions' => $mobileTransactions,
            'successMobile' => $successMobile,
            'dthTransactions' => $dthTransactions,
            'postPaidTransactions' => $postPaidTransactions,
            'walletTransactions' => $walletTransactions,
            'successPostPaid' => $successPostPaid,
            'successDth' => $successDth,
            'users' => $users
        ]);
    }
    public function dthTransactions()
    {
        $dthTransactions = Dthtransaction::orderBy('id','DESC')->get();
        return view('admin.transactions.dthTransactions',['transactions'=>$dthTransactions]);
    }
    public function mobileTransactions()
    {
        $mobileTransactions = Mobiletransaction::orderBy('id','DESC')->get();
        return view('admin.transactions.mobileTransactions',['transactions'=>$mobileTransactions]);
    }
    public function postPaidTransactions()
    {
        $postPaidTransactions = Postpaidtransaction::orderBy('id','DESC')->get();
        return view('admin.transactions.postPaidTransactions',['transactions'=>$postPaidTransactions]);
    }
    public function walletTransactions()
    {
        $walletTransaction = Transaction::orderBy('id','DESC')->get();
        return view('admin.transactions.walletTransactions',['transactions'=>$walletTransaction]);
    }

}
