<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name',
      'email',
      'password',
      'address',
      'role',
      'salutation',
      'mobile',
      'otp',
      'city',
      'zip_code',
      'state',
      'date_of_birth',
    'country',
    'lasttry',
      'shop_owner',
    'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'password',
      'remember_token',
    ];

    public function transactions () {
        return $this->hasMany('App\models\Transaction');
    }
    public function mobiletransactions () {
        return $this->hasMany('App\models\Mobiletransaction');
    }
    
    public function successTransactions()
    {
        return $this->hasMany('App\Models\Transaction','user_id')->where('order_status','Success');
    }

    public function moneyAddedByAdmin()
    {
        return $this->hasMany('App\Models\CashbackTracking','user_id');
    }

    public function postPaidRecharge()
    {
        return $this->hasMany('App\Models\Postpaidtransaction','user_id')->where('status','SUCCESS');
    }

    public function mobileRecharge()
    {
        return $this->hasMany('App\Models\Mobiletransaction','user_id')->where('status','SUCCESS');
    }

    public function dthRecharge()
    {
        return $this->hasMany('App\Models\Dthtransaction','user_id')->where('status','SUCCESS');
    }
}
