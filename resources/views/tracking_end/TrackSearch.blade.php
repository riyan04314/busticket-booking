@extends('layouts.system')
@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{url('/admin/trackadd')}}">Tracking/Search & Update</a>
            <i class="fa fa-circle"></i>
        </li>

    </ul>

</div>
<h1 class="page-title"> Track Search & Update Page
    <small>You can search the data for you or update it.</small>
</h1>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Update & Search</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th> Track No </th>
                            <th> Destination </th>
                            <th> CompanyName(Sender) </th>
                            <th> Location(Sender) </th>
                            <th> Name(Reciever) </th>
                            <th> Address(Reciever) </th>
                            <th> Ship No </th>
                            <th> COD am(shipment) </th>
                            <th> Value(shipment) </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                          @foreach($track_data as $data)
                        <tr>
                            <td> {{$data->tracking_no}} </td>
                            <td> {{$data->destination}} </td>
                            <td> {{$data->s_company_name}} </td>
                            <td> {{$data->s_location}} </td>
                            <td> {{$data->r_name}} </td>
                            <td> {{$data->r_adress}} </td>
                            <td> {{$data->sh_no}} </td>
                            <td> {{$data->sh_cod_amount}} </td>
                            <td> {{$data->sh_cus_value}} </td>
                            <td>
                                <button onclick="editTrack('{{$data->id}}')"><i class="fa fa-edit text-primary"></i></button> 
                                <button onclick="removeTrack('{{$data->id}}')"><i class="fa fa-remove text-danger"></i></button> 
                            </td> 
                        </tr>
                        @endforeach    
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="editTrackModel"></div>

<script type="text/javascript">
    function removeTrack($id) {
        var id = $id;
        var r = confirm("Are you Sure!");
        if (r == true) {
            $.get('{{URL::to("admin/deletePackage")}}', { id:id }, function(data){
                if (data == 'success') {
                    location.reload();
                }
            })
        } else {
            return false;
        } 
        
    }

    function editTrack($id){
        var id = $id;
        $.get('{{URL::to("admin/getPackage")}}', { id:id }, function(data){
            $('#editTrackModel').empty().html(data);           
            $('#edit_track_modal').modal('show');
        })
    }
</script>


@endsection
