@extends('layouts.app_admin')

@section('content')

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Coupons
        	<small>Preview page</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Coupons</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
        	<div class="col-md-12">
            	<div class="box box-success">
                	<div class="box-header">
                    	<h3 class="box-title">Coupons</h3> 
                	</div>
                	<div class="box-body">

                        <div class="table-responsive"> 
                            <table id="example" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Discount Type</th>
                                        <th>C.B Type</th>
                                        <th>Flat Discount</th>
                                        <th>Discount (%)</th>
                                        <th>Max Discount</th>
                                        <th>Flat Cash Back</th>
                                        <th>Cash Back (%)</th>
                                        <th>Max Cash Back</th>
                                        <th>Gift Name</th>
                                        <th>Usage Limit</th>
                                        <th>Description</th>
                                        <th>Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $i = 0;
                                ?>
                                @foreach($coupons as $copon)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $copon->coupon_code }}</td>
                                        <td>{{ $copon->coupon_name }}</td>
                                        <td>{{ $copon->coupon_type }}</td>
                                        <td>{{ $copon->start_date }}</td>
                                        <td>{{ $copon->end_date }}</td>
                                        <td>{{ $copon->discount_type }}</td>
                                        <td>{{ $copon->cash_back_type }}</td>
                                        <td>{{ $copon->flat_discount }}</td>
                                        <td>{{ $copon->per_discount }}</td>
                                        <td>{{ $copon->max_discount }}</td>
                                        <td>{{ $copon->flat_cash_back }}</td>
                                        <td>{{ $copon->per_cash_back }}</td>
                                        <td>{{ $copon->max_cash_back }}</td>
                                        <td>{{ $copon->gift_name }}</td>
                                        <td>{{ $copon->usage_limit }}</td> 
                                        <td>{{ $copon->description }}</td>
                                        @if($copon->image == null)
                                            <td></td>
                                        @else
                                            <td><img src="{{URL::asset('/uploads/'.$copon->image)}}" alt="profile Pic" height="50" width="50"></td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                	</div>
          		</div>
        	</div>
      	</div>
    </section>

</div>

@endsection

@section('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
@endsection


@section('script')
    <script src="{{ asset('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection