
@extends('layouts.system')
@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{url('/admin')}}">Home</a>
            <i class="fa fa-circle"></i>
        </li>

    </ul>

</div>

<h1 class="page-title"> Admin Dashboard
    <small>Tracking, Manage users</small>
</h1>

@endsection