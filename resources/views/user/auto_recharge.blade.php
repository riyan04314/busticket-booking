@extends('blades.base')

{{-- @section('page title', 'Index') --}}

@section('innerbanner')
    <!-- innerbanner -->
    {{-- <div class="agileits-inner-banner">

    </div> --}}
    <!-- //innerbanner -->
@endsection

@section('breadcrumbs')
@endsection

@section('horizontal tab')

@endsection
@section('css')
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/travel.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('css/tool-tip.css')}}">
    <link rel="stylesheet" href="{{ asset('css/myprofile-style.css')}}">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
@endsection

@section('vertical tab')
    <div class="container">
        <h2>Auto Recharge</h2>
        <div class="profile">
            <!--<div class="row">-->
            <!--    <div class="col-md-12 col-lg-12">-->
            <!--        <div class="text-center">-->
            <!--            <img class="avatar" src="/images/profile-images/avatar.png" alt="">-->
            <!--            <p class="username">{{ $user->name }}</p>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->

            <div class="row">
                <div class="col-md-6 h-100">
                    <!-- <ul class="nav nav-pills nav-stacked p-tabs">
                        <li class="active"><a data-toggle="tab" href="#profile">Profile</a></li>
                        <li><a data-toggle="tab" href="#security1">Security</a></li>
                        <li><a href="/myprofile/wallet">Wallet</a></li>
                    </ul> -->
                    <h2>Recherge Info</h2>

					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

				</div>
                <div class="col-md-6">
                    <div class="tab-content">
                        <div id="profile" class="tab-pane fade in active">
                            <div class="p-container">
                                <form class="form-horizontal" method="POST" action="{{ URL::to('auto-recharge-store') }}">
                                    {{ csrf_field() }}
                                    <div class="required form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
								        <label class="control-label col-xs-3 col-sm-3" for="mobile">Mobile:</label>
								        <div class="col-xs-9 col-sm-9">
							    	        <input type='text' class="form-control"  id='mobile' placeholder="Enter Mobile" name="mobile" required="required" />
							    	        <span class="text-danger">
							    			    {{ $errors->first('mobile') }}
							    		    </span>
								        </div>
									</div>
									
									<div class="required form-group {{ $errors->has('balance') ? 'has-error' : ''}}">
								        <label class="control-label col-xs-3 col-sm-3" for="balance">Recharge:</label>
								        <div class="col-xs-9 col-sm-9">
							    	        <input type='number' class="form-control"  id='balance' placeholder="Enter Recharge" name="balance" required="required" />
							    	        <span class="text-danger">
							    			    {{ $errors->first('balance') }}
							    		    </span>
								        </div>
									</div>
									
									<div class="required form-group {{ $errors->has('recharge_date') ? 'has-error' : ''}}">
								        <label class="control-label col-xs-3 col-sm-3" for="recharge_date">Date:</label>
								        <div class="col-xs-9 col-sm-9">
							    	        <input type='text' class="form-control"  id='datetimepicker1' placeholder="Enter Date" name="recharge_date" required="required" />
							    	        <span class="text-danger">
							    			    {{ $errors->first('recharge_date') }}
							    		    </span>
								        </div>
									</div>
									
									<div class="required form-group {{ $errors->has('recharge_time') ? 'has-error' : ''}}">
								        <label class="control-label col-xs-3 col-sm-3" for="recharge_time">Time:</label>
								        <div class="col-xs-9 col-sm-9">
							    	        <input type='text' class="form-control"  id='datetimepicker3' placeholder="Enter Time" name="recharge_time" required="required" />
							    	        <span class="text-danger">
							    			    {{ $errors->first('recharge_time') }}
							    		    </span>
								        </div>
									</div>
									
									<div class="required form-group {{ $errors->has('operator') ? 'has-error' : ''}}">
								        <label class="control-label col-xs-3 col-sm-3" for="operator">Operator:</label>
								        <div class="col-xs-9 col-sm-9">
							    	        <!-- <input type='text' class="form-control"  id='operator' placeholder="Enter Operator" name="operator" required="required" /> -->
							    	        <select class="form-control" id='operator' placeholder="Enter Operator" name="operator" required="required">
                                                <option value="">Select Operator</option>
                                                @foreach($prepaidOperators as $prepaidOperator)
                                                    <option value="{{ $prepaidOperator->name }}">{{ $prepaidOperator->name }}</option>
                                                @endforeach
                                            </select>
							    	        <span class="text-danger">
							    			    {{ $errors->first('operator') }}
							    		    </span>
								        </div>
									</div>
									
									<div class="required form-group {{ $errors->has('circle') ? 'has-error' : ''}}">
								        <label class="control-label col-xs-3 col-sm-3" for="circle">Circle:</label>
								        <div class="col-xs-9 col-sm-9">
							    	        <input type='text' class="form-control"  id='circle' placeholder="Enter Circle" name="circle" required="required" />
							    	        <span class="text-danger">
							    			    {{ $errors->first('circle') }}
							    		    </span>
								        </div>
									</div>
									
                                    <!-- <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Your Email:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info">{{ $user->email }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Phone Number:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info">{{$user->mobile}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Your Address:</label>
                                        <div class="col-sm-10 ">
                                            <div class="under-line">
                                                <p class="text-info">{{$user->address}} </p>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-7">
                                            <button class="btn btn-primary" type="submit">Auto Recharge</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="security" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p>Some content in menu 1.</p>
                        </div>
                        <div id="wallet" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p>Some content in menu 2.</p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <h2>Current Auto Recharges</h2>
                <div class="panel panel-info">
    	            <div class="panel-heading">
    	                <h3 class="panel-title text-center"><i class="fa fa-list-ul"></i> List of <code><b>Current Auto Recharges</b></code></h3>
    	            </div>
    	            <div class="panel-body">
    	            	<div class="table-responsive">
    		                <table id="myTable" class="table table-bordered table-striped table-hover">
    		                    <thead>
    		                        <tr class="success">
    		                            <th>SL</th>
    		                            <th>Mobile</th>
    		                            <th>Amount</th>
    		                            <th>Operator</th>
    		                            <th>Circle</th>
    		                            <th>Recharge At</th>
    		                        </tr>
    		                    </thead>
    		                    <tbody>
    		                    <?php
    		                        $i = 0;
    		                    ?>
    		                    @foreach($pendinRecharges as $pendinRecharge)
    		                        <tr>
    		                            <td>{{ ++$i }}</td>
    		                            <td>{{ $pendinRecharge->mobile }}</td>
    		                            <td><strong>{{ $pendinRecharge->balance }}</strong></td>
    		                            <td>{{ $pendinRecharge->operator }}</td>
    		                            <td>{{ $pendinRecharge->circle }}</td>
    		                            <td>{{ $pendinRecharge->recharged_at }}</td>
    		                        </tr>
    		                    @endforeach
    		                    </tbody>
    		                </table>
    	            	</div>
    	            </div>
	            </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-4 col-xs-offset-4">
                <a href="{{ url('/show-pre-recharge') }}" class="btn btn-primary">View Previous Auto Recharge Log</a>
                </div>
            </div>
        </div>
     
    </div>
@endsection

@section('tab title')
    Plan
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            //Vertical Tab
            $('#parentVerticalTab').easyResponsiveTabs({
                type: 'vertical', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function(event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo2');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#tab2").hide();
            $("#tab3").hide();
            $("#tab4").hide();
            $(".tabs-menu a").click(function(event){
                event.preventDefault();
                var tab=$(this).attr("href");
                $(".tab-grid").not(tab).css("display","none");
                $(tab).fadeIn("slow");
            });
        });
    </script>
    <script src="/js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#datepicker,#datepicker1" ).datepicker();
        });
    </script>
    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript">
        $(function () {
            $('#datetimepicker3').datetimepicker({
                format: 'HH:mm'
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: "YYYY-MM-DD"
            });
        });
    </script>
@endsection

@section('header-right')
    <div class=" header-right">
        <div class="banner">
            <s-banner></s-banner>
        </div>
    </div>
@endsection


