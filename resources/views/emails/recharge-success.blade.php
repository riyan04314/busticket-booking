<!DOCTYPE html>
<html lang="en">
<body>
    <style>
        .invoice-detail {
            font-size: 17px;
            font-weight: bold;
            padding: 6px;
        }
    </style>
    <div style="    min-width: 500px;max-width: 70%;margin: auto;font-family: Myriad Pro;color: #3f3f3f;">
        <img src="https://www.zapwallet.in/images/invoice_top.png" alt="" style="width:100%;display: block">
        <div style="    width: 100%;background-image: url(https://www.zapwallet.in/images/invoice_bg.jpg);background-size: cover;;display: block">
            <div style="padding-left: 30px;">
                <h2 style="margin: 0;padding-top: 1em;padding-bottom: 30px;"> Below are your recharge details!</h2>
                <div class="invoice-detail"> <img src="https://www.zapwallet.in/images/invoice-option.png" alt=""> &nbsp;&nbsp;ORDER NUMBER:  <span style="font-weight: 100;"> ................................................................  {{$order_no}}</span></div>
                <div class="invoice-detail"> <img src="https://www.zapwallet.in/images/invoice-option.png" alt=""> &nbsp;&nbsp;RECHARGE TYPE:  <span style="font-weight: 100;"> ................................................................  {{$type}}</span></div>
                <div class="invoice-detail"> <img src="https://www.zapwallet.in/images/invoice-option.png" alt=""> &nbsp;&nbsp;OPERATOR:  <span style="font-weight: 100;"> ............................................................................  {{$opt}}</span></div>
                <div class="invoice-detail"> <img src="https://www.zapwallet.in/images/invoice-option.png" alt=""> &nbsp;&nbsp;MOBILE NUMBER:  <span style="font-weight: 100;"> ..............................................................  {{$number}}</span></div>
                <div class="invoice-detail"> <img src="https://www.zapwallet.in/images/invoice-option.png" alt=""> &nbsp;&nbsp;RECHARGE AMOUNT:  <span style="font-weight: 100;"> .......................................................   ₹ {{$amount}}</span></div>
            </div>
        </div>
        <img src="https://www.zapwallet.in/images/invoice_bottom.png" alt="" style="width:100%;display: block">
    </div>
</body>
</html>