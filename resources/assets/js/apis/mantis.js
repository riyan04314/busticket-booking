const root_uri = 'http://api.iamgds.com/ota/'

export default {
    auth(completion, error) {
        axios.get('/mantis/authentication')
            .then(response => {
                window.axios.defaults.headers.common['access-token'] = response.data;
                if(completion) {
                    completion(response.data);
                }
            })
            .catch(err => {
                if(error) {
                    error(err);
                }
            });
    },

    cityList(completion, error) {
        axios.get(root_uri + 'CityList')
            .then(response => {
                completion(response.data);
            })
            .catch(err => {
                if(error) {
                    error(err);
                }
            });
    },

    search(params,completion, error) {
        axios.get(root_uri + 'Search', params)
            .then(response => {
                completion(response.data);
            })
            .catch(err => {
                if(error) {
                    error(err);
                }
            });
    },

    detail(params,completion, error) {
        axios.get(root_uri + 'Chart', params)
            .then(response => {
                completion(response.data);
            })
            .catch(err => {
                if(error) {
                    error(err);
                }
            });
    }
}